# Using PHP_CodeSniffer and PHP Code Beautifier and Fixer

## Introduction
This guide provides instructions on how to use PHP_CodeSniffer (PHPCS) and PHP Code Beautifier and Fixer (PHPCBF) to maintain coding standards in your PHP projects.


### Running PHP_CodeSniffer (PHPCS)
1. Navigate to your project directory.
2. Run PHP_CodeSniffer with your desired coding standard:
    ```shell
    ./vendor/bin/phpcs
    ```

### Running PHP Code Beautifier and Fixer (PHPCBF)
1. Navigate to your project directory.
2. Use PHP Code Beautifier and Fixer to automatically fix coding standard violations:
    ```shell
    ./vendor/bin/phpcbf
    ```

## Additional Information
- For more detailed documentation on PHP_CodeSniffer (PHPCS), visit: [PHP_CodeSniffer Documentation](https://github.com/squizlabs/PHP_CodeSniffer).
- For more detailed documentation on PHP Code Beautifier and Fixer (PHPCBF), visit: [PHP CS Fixer Documentation](https://github.com/FriendsOfPHP/PHP-CS-Fixer).

## Contributing
Please feel free to contribute by submitting issues or pull requests.

## License
This project is licensed under the [MIT License](LICENSE).
