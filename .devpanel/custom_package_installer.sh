#!/bin/bash
# ---------------------------------------------------------------------
# Copyright (C) 2023 DevPanel
# You can install any service here to support your project
# Please make sure you run apt update before install any packages
# Example:
# - sudo apt-get update
# - sudo apt-get install nano
#
# ----------------------------------------------------------------------

sudo apt update
sudo apt-get install -y nano cron
if [[ -f "$APP_ROOT/.devpanel/cron_scripts/main_cron.sh" ]]; then
    sudo rm -rf ~/.env
    /usr/bin/env | grep "DB\|APP\|WEB" > ~/.env
    sudo mkdir -p $APP_ROOT/.devpanel/.logs/
    sudo touch $APP_ROOT/.devpanel/.logs/cron.log
    sudo chown www:www -R  $APP_ROOT/.devpanel/.logs
    sudo chmod 755 -R $APP_ROOT/.devpanel/.logs
    cat $APP_ROOT/.devpanel/cron_scripts/main_cron.sh | crontab -u www -
    sudo service cron start
fi


# Download and install Node.js and npm
curl -sL https://deb.nodesource.com/setup_18.x | sudo -E bash -
sudo apt install -y nodejs

# Verify the installation
node -v
npm -v
sudo npm install --global gulp-cli
