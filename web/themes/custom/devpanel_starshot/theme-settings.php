<?php

/**
 * @file
 * Theme settings form for Devpanel  Starshot theme.
 */

/**
 * Implements hook_form_system_theme_settings_alter().
 */
function devpanel_starshot_form_system_theme_settings_alter(&$form, &$form_state) {

  $form['devpanel_starshot'] = [
    '#type' => 'details',
    '#title' => t('Devpanel  Starshot'),
    '#open' => TRUE,
  ];

  $form['devpanel_starshot']['font_size'] = [
    '#type' => 'number',
    '#title' => t('Font size'),
    '#min' => 12,
    '#max' => 18,
    '#default_value' => theme_get_setting('font_size'),
  ];

}
