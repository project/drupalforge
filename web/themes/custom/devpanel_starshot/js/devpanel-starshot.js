/**
 * @file
 * JavaScript file for the starshot core module.
 */

 (function (Drupal, once, $) {
  'use strict';

  var StarshotTheme = {};

  StarshotTheme.init = function(context) {
    // StarshotTheme.countdown();
    StarshotTheme.context = context;
    StarshotTheme.menu();
    StarshotTheme.textAnimate();
    StarshotTheme.accountBlock();
    StarshotTheme.mbTableReponsive();
  }
  StarshotTheme.accountBlock  = function() {
    $(once('init', '.sso-account',  StarshotTheme.context)).each(function () {
      $('.svg-icon').click(function() {
        $('.sso-account').toggleClass('show');
      });
    });
  }

  StarshotTheme.menu = function() {
    $(once('navbar', '.navbar',  StarshotTheme.context)).each(function () {
      let element = $(this);
      $('.navbar-toggler', element).click(function() {
        $('.navbar-collapse', element).toggleClass('show');
      });
    });

  }
  StarshotTheme.textAnimate = function () {
    // Wrap every letter in a span

    var textWrappers = document.querySelectorAll('.ml12');
    textWrappers.forEach(function(textWrapper) {
      let text = textWrapper.textContent;

      text = text.replaceAll(/([\w!@#$%^&*(),.?":{}|<>-]+)/g, '<span class="letter">$&</span>');
      // text = '<span class="letter">' + text + '</span>';
      textWrapper.innerHTML = text;
      anime.timeline({loop: false})
      .add({
        targets: '.ml12 .letter',
        translateX: [40,0],
        translateZ: 0,
        opacity: [0,1],
        easing: "easeOutExpo",
        duration: 1200,
        delay: (el, i) => 500 + 30 * i,
        begin: function() {
          textWrapper.classList.add("loadded");
        }
      });
    })

  }
  StarshotTheme.mbTableReponsive = function () {
    $(once('init', '.mb-table-responsive .table-responsive',  StarshotTheme.context)).each(function () {
      let table = $(this);
      table.slideUp(0);
      $('thead th', table).each(function(){
        var index = $(this).index(); 
        let th = $(this);
        $('tbody tr', table).each(function(){
          let td = $('td:nth-child('+ (index + 1) +')',$(this));
          if(td.length){
            let td_html =  `<span class="mb-display">`+th.html()+`</span><span class="mb-align-right">`+td.html()+`</span>`;
            td.html(td_html);
          }
        });
      })
      table.slideDown(500);
    });
  }
  Drupal.behaviors.StarshotTheme = {
    attach(context) {
      StarshotTheme.init(context);
    }
  }
})(Drupal, once, jQuery);
