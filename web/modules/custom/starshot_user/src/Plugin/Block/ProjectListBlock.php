<?php

declare(strict_types=1);

namespace Drupal\starshot_user\Plugin\Block;

use Drupal\Component\Render\FormattableMarkup;
use Drupal\Core\Block\BlockBase;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a project list block.
 *
 * @Block(
 *   id = "project_list",
 *   admin_label = @Translation("Project list"),
 *   category = @Translation("Custom"),
 * )
 */
final class ProjectListBlock extends BlockBase implements ContainerFactoryPluginInterface {
  /**
   * The related request stack.
   *
   * @var \Symfony\Component\HttpFoundation\RequestStack
   */
  protected $requestStack;

  /**
   * The entity type manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The config factory to load config from.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;
  /**
   * The current user.
   *
   * @var \Drupal\Core\Session\AccountProxyInterface
   */
  protected $currentUser;
  /**
   * The pagerManager.
   *
   * @var \Drupal\Core\Pager\PagerManager
   */
  protected $pagerManager;
  /**
   * The $pagerParameters.
   *
   * @var \Drupal\Core\Pager\PagerManager
   */
  protected $pagerParameters;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    $instance = new static($configuration, $plugin_id, $plugin_definition);
    $instance->entityTypeManager = $container->get('entity_type.manager');
    $instance->requestStack = $container->get('request_stack');
    $instance->configFactory = $container->get('config.factory');
    $instance->httpClient = $container->get('http_client');
    $instance->currentUser = $container->get('current_user');
    $instance->pagerManager = $container->get('pager.manager');
    $instance->pagerParameters = $container->get('pager.parameters');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function build(): array {
    $build = [];
    $build['#cache'] = ['max-age' => 0];

    if (!$this->currentUser->isAuthenticated()) {
      return $build;
    }
    $limit = 10;
    $current_page = $this->pagerParameters->findPage();
    $user_entity = $this->entityTypeManager->getStorage('user')->load($this->currentUser->id());
    $endpoint = $this->configFactory->get('starshot_core.settings')->get('apiEndpoint');
    $email = $user_entity->getEmail();
    $endpoint .= '/api/v2/drupal-forge/application/list?email=' . urlencode($email) . '&pageIndex=' . $current_page . '&limit=' . $limit;

    $accessToken = $this->configFactory->get('starshot_core.settings')->get('apiToken');
    try {
      // Number of items to display per page.
      $response = $this->httpClient->request('GET', $endpoint, [
        'headers' => [
          'Authorization' => $accessToken,
          'Content-Type' => 'application/json',
        ],
      ]);

      $response = $response->getBody()->getContents();
      $response = json_decode($response);
      $total = $response->total;
      $data = $response->applications;
      $this->pagerManager->createPager($total, $limit);

    }
    catch (\Exception $e) {
      if ($this->currentUser->hasPermission('administer site configuration')) {
        $error_message = $e->getMessage();
        $this->messenger()->addMessage($error_message, 'error');
      }
      return $build;
    }
    $header = [
      ['data' => $this->t('ID'), 'field' => 'column-id', 'class' => ['column-id']],
      ['data' => $this->t('Project Name'), 'field' => 'column-project-name', 'class' => ['column-project-name']],
      ['data' => $this->t('Branch'), 'field' => 'column-branch', 'class' => ['column-branch']],
      ['data' => $this->t('Status'), 'field' => 'column-status', 'class' => ['column-status']],
      ['data' => $this->t('Container size'), 'field' => 'column-size', 'class' => ['column-size']],
      ['data' => $this->t('Last used'), 'field' => 'column-last-use', 'class' => ['column-last-use']],
      ['data' => $this->t('Expired At'), 'field' => 'column-expire-at', 'class' => ['column-expire-at']],
      ['data' => $this->t('Delete At'), 'field' => 'column-delete-at', 'class' => ['column-delete-at']],
      ['data' => $this->t('Actions'), 'field' => 'col8', 'class' => ['column-actions']],
    ];
    $rows = [];
    foreach ($data as $item) {
      $links = [
        'workspace' => isset($item->workspaceUrl) ? '<li><a href="' . $item->workspaceUrl . '" target="_blank">' . $this->t('Workspace') . '</a></li>' : '',
        'view' => isset($item->hostname) ? '<li><a href="http://' . $item->hostname . '" target="_blank">' . $this->t('Application') . '</a></li>' : '',
        'extend' => isset($item->_id) ? '<li><a href="' . Url::fromRoute('starshot_stripe.payment_application', ['app_id' => $item->_id])->toString() . '">' . $this->t('Extend') . '</a></li>' : '',
      ];
      $dropdown_html = '<div class="dropdown">
                          <button class="_btn dropdown-toggle" type="button" data-toggle="dropdown">Actions
                          <span class="caret"></span></button>
                          <ul class="dropdown-menu">
                            ' . implode('', $links) . '
                          </ul>
                        </div>';
      $status = '';
      if (isset($item->status)) {
        $status = $item->status;
        if ($item->status == 'DEPLOY_APPLICATION_SUCCESS') {
          if (empty($item->replicas)) {
            $status = 'PAUSED';
          }
          else {
            $status = 'RUNNING';
          }
        }
      }
      $rows[] = [
        'data' => [
          'column-id' => [
            'data' => $item->_id ?? '',
            'class' => 'column-id',
          ],
          'column-project-name' => [
            'data' => $item->project->name ?? '',
            'class' => 'column-project-name',
          ],
          'column-branch' => [
            'data' => $item->name ?? '',
            'class' => 'column-branch',
          ],
          'column-status' => [
            'data' => $status,
            'class' => 'column-status',
          ],
          'column-size' => [
            'data' => $item->capacityLimit ?? '',
            'class' => 'column-size',
          ],
          'column-last-use' => [
            'data' => isset($item->updatedAt) ? $this->convertJsDateToDrupalDate($item->updatedAt) : '',
            'class' => 'column-last-use',
          ],
          'column-expire-at' => [
            'data' => isset($item->expiredTime) ? $this->convertJsDateToDrupalDate($item->expiredTime) : '',
            'class' => 'column-expire-at',
          ],
          'column-delete-at' => [
            'data' => isset($item->drupalForge->deleteAt) ? $this->convertJsDateToDrupalDate($item->drupalForge->deleteAt) : '',
            'class' => 'column-delete-at',
          ],
          'column-actions' => [
            'data' => new FormattableMarkup($dropdown_html, []),
            'class' => 'column-actions',
          ],
        ],
      ];
    }

    $build['project_list'] = [
      '#type' => 'table',
      '#header' => $header,
      '#rows' => $rows,
      '#empty' => $this->t('No data available.'),
    ];

    $build['pager'] = [
      '#type' => 'pager',
    ];

    return $build;
  }

  /**
   * Convert Date from API to display on drupal.
   */
  protected function convertJsDateToDrupalDate($dateString) {
    $user_entity = $this->entityTypeManager->getStorage('user')->load($this->currentUser->id());
    $timezone = $user_entity->getTimezone();
    if (empty($timezone)) {
      $timezone = $this->configFactory->get('system.date')->get('timezone.default');
    }
    $date = new \DateTime($dateString, new \DateTimeZone('UTC'));
    $date->setTimezone(new \DateTimeZone($timezone));

    $formatted_date = $date->format('Y-m-d H:i');
    return $formatted_date;
  }

}
