<?php

declare(strict_types=1);

namespace Drupal\starshot_stripe\Entity;

use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\starshot_stripe\TransactionInterface;
use Drupal\user\EntityOwnerTrait;

/**
 * Defines the transaction entity class.
 *
 * @ContentEntityType(
 *   id = "transaction",
 *   label = @Translation("Transaction"),
 *   label_collection = @Translation("Transactions"),
 *   label_singular = @Translation("transaction"),
 *   label_plural = @Translation("transactions"),
 *   label_count = @PluralTranslation(
 *     singular = "@count transactions",
 *     plural = "@count transactions",
 *   ),
 *   handlers = {
 *     "list_builder" = "Drupal\starshot_stripe\TransactionListBuilder",
 *     "views_data" = "Drupal\views\EntityViewsData",
 *     "access" = "Drupal\starshot_stripe\TransactionAccessControlHandler",
 *     "form" = {
 *       "add" = "Drupal\starshot_stripe\Form\TransactionForm",
 *       "edit" = "Drupal\starshot_stripe\Form\TransactionForm",
 *       "delete" = "Drupal\Core\Entity\ContentEntityDeleteForm",
 *       "delete-multiple-confirm" = "Drupal\Core\Entity\Form\DeleteMultipleForm",
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\Core\Entity\Routing\AdminHtmlRouteProvider",
 *     },
 *   },
 *   base_table = "transaction",
 *   admin_permission = "administer transaction",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "id",
 *     "uuid" = "uuid",
 *     "owner" = "uid",
 *   },
 *   links = {
 *     "collection" = "/admin/content/transaction",
 *     "add-form" = "/transaction/add",
 *     "canonical" = "/transaction/{transaction}",
 *     "edit-form" = "/transaction/{transaction}/edit",
 *     "delete-form" = "/transaction/{transaction}/delete",
 *     "delete-multiple-form" = "/admin/content/transaction/delete-multiple",
 *   },
 *   field_ui_base_route = "entity.transaction.settings",
 * )
 */
final class Transaction extends ContentEntityBase implements TransactionInterface {

  use EntityOwnerTrait;

  /**
   * {@inheritdoc}
   */
  public function preSave(EntityStorageInterface $storage): void {
    parent::preSave($storage);
    if (!$this->getOwnerId()) {
      // If no owner has been set explicitly, make the anonymous user the owner.
      $this->setOwnerId(0);
    }
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type): array {

    $fields = parent::baseFieldDefinitions($entity_type);
    $fields['uid'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Author'))
      ->setSetting('target_type', 'user')
      ->setDefaultValueCallback(self::class . '::getDefaultEntityOwner')
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'author',
        'weight' => 15,
      ])
      ->setDisplayConfigurable('view', TRUE);

    $fields['app_id'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Application ID'))
      ->setRequired(TRUE)
      ->setDefaultValue('')
      ->setSetting('max_length', 255)
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => -5,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['subscription_id'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Subscription ID'))
      ->setRequired(TRUE)
      ->setDefaultValue('')
      ->setSetting('max_length', 255)
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => -5,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['subscription_status'] = BaseFieldDefinition::create('list_string')
      ->setLabel(t('Subscription status'))
      ->setDescription(t('Subscription status from stripe'))
      ->setSettings([
        'allowed_values' => [
          'active' => t('Active'),
          'canceled' => t('Canceled'),
        ],
      ])
      ->setDisplayOptions('view', [
        'label' => 'visible',
        'type' => 'list_default',
        'weight' => 6,
      ])
      ->setDisplayOptions('form', [
        'type' => 'options_select',
        'weight' => 6,
      ])
      ->setDisplayConfigurable('view', TRUE)
      ->setDisplayConfigurable('form', TRUE);
    $fields['amount'] = BaseFieldDefinition::create('float')
      ->setLabel(t('Amount'))
      ->setDescription(t('Amount of transaction'))
      ->setDefaultValue(0)
      ->setDisplayOptions('view', [
        'label' => 'visible',
        'type' => 'list_default',
        'weight' => 6,
      ])
      ->setDisplayOptions('form', [
        'type' => 'options_select',
        'weight' => 6,
      ])
      ->setDisplayConfigurable('view', TRUE)
      ->setDisplayConfigurable('form', TRUE);

    $fields['currency'] = BaseFieldDefinition::create('list_string')
      ->setLabel(t('Currency'))
      ->setDescription(t('Payment currency'))
      ->setSettings([
        'allowed_values' => [
          'usd' => t('USD'),
        ],
      ])
      ->setDisplayOptions('view', [
        'label' => 'visible',
        'type' => 'list_default',
        'weight' => 6,
      ])
      ->setDisplayOptions('form', [
        'type' => 'options_select',
        'weight' => 6,
      ])
      ->setDisplayConfigurable('view', TRUE)
      ->setDisplayConfigurable('form', TRUE);

    $fields['period'] = BaseFieldDefinition::create('list_string')
      ->setLabel(t('Subscription period'))
      ->setDescription(t('Subscription period'))
      ->setSettings([
        'allowed_values' => [
          'day' => t('Daily'),
          'month' => t('Monthly'),
          'year' => t('Yearly'),
        ],
      ])
      ->setDisplayOptions('view', [
        'label' => 'visible',
        'type' => 'list_default',
        'weight' => 6,
      ])
      ->setDisplayOptions('form', [
        'type' => 'options_select',
        'weight' => 6,
      ])
      ->setDisplayConfigurable('view', TRUE)
      ->setDisplayConfigurable('form', TRUE);

    $fields['container_size'] = BaseFieldDefinition::create('list_string')
      ->setLabel(t('Container size'))
      ->setDescription(t('Container size'))
      ->setSettings([
        'allowed_values' => [
          'small_1' => t('small_1'),
          'medium_1' => t('medium_1'),
          'large_1' => t('large_1'),
        ],
      ])
      ->setDisplayOptions('view', [
        'label' => 'visible',
        'type' => 'list_default',
        'weight' => 6,
      ])
      ->setDisplayOptions('form', [
        'type' => 'options_select',
        'weight' => 6,
      ])
      ->setDisplayConfigurable('view', TRUE)
      ->setDisplayConfigurable('form', TRUE);

    $fields['payment_status'] = BaseFieldDefinition::create('list_string')
      ->setLabel(t('Payment status'))
      ->setDescription(t('Payment status from stripe'))
      ->setSettings([
        'allowed_values' => [
          'succeeded' => t('Succeeded'),
          'processing' => t('Processing'),
          'requires_payment_method' => t('Requires payment method'),
        ],
      ])
      ->setDisplayOptions('view', [
        'label' => 'visible',
        'type' => 'list_default',
        'weight' => 6,
      ])
      ->setDisplayOptions('form', [
        'type' => 'options_select',
        'weight' => 6,
      ])
      ->setDisplayConfigurable('view', TRUE)
      ->setDisplayConfigurable('form', TRUE);

    $fields['subscription_ended'] = BaseFieldDefinition::create('timestamp')
      ->setLabel(t('Ended'))
      ->setDescription(t('The time that the transaction was ended.'))
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'timestamp',
        'weight' => 20,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('form', [
        'type' => 'datetime_timestamp',
        'weight' => 100,
      ])
      ->setDisplayConfigurable('view', TRUE);

    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Created'))
      ->setDescription(t('The time that the transaction was created.'));

    $fields['updated'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Updated'))
      ->setDescription(t('The time that the transaction was updated.'));

    $fields['expired'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Expired'))
      ->setDescription(t('The time that the transaction was expired.'));

    return $fields;
  }

}
