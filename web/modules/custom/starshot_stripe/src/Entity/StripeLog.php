<?php

declare(strict_types=1);

namespace Drupal\starshot_stripe\Entity;

use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\starshot_stripe\StripeLogInterface;
use Drupal\user\EntityOwnerTrait;

/**
 * Defines the stripe log entity class.
 *
 * @ContentEntityType(
 *   id = "stripe_log",
 *   label = @Translation("Stripe log"),
 *   label_collection = @Translation("Stripe logs"),
 *   label_singular = @Translation("stripe log"),
 *   label_plural = @Translation("stripe logs"),
 *   label_count = @PluralTranslation(
 *     singular = "@count stripe logs",
 *     plural = "@count stripe logs",
 *   ),
 *   handlers = {
 *     "list_builder" = "Drupal\starshot_stripe\StripeLogListBuilder",
 *     "views_data" = "Drupal\views\EntityViewsData",
 *     "access" = "Drupal\starshot_stripe\StripeLogAccessControlHandler",
 *     "form" = {
 *       "add" = "Drupal\starshot_stripe\Form\StripeLogForm",
 *       "edit" = "Drupal\starshot_stripe\Form\StripeLogForm",
 *       "delete" = "Drupal\Core\Entity\ContentEntityDeleteForm",
 *       "delete-multiple-confirm" = "Drupal\Core\Entity\Form\DeleteMultipleForm",
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\Core\Entity\Routing\AdminHtmlRouteProvider",
 *     },
 *   },
 *   base_table = "stripe_log",
 *   admin_permission = "administer stripe_log",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "id",
 *     "uuid" = "uuid",
 *     "owner" = "uid",
 *   },
 *   links = {
 *     "collection" = "/admin/content/stripe-log",
 *     "add-form" = "/stripe-log/add",
 *     "canonical" = "/stripe-log/{stripe_log}",
 *     "edit-form" = "/stripe-log/{stripe_log}/edit",
 *     "delete-form" = "/stripe-log/{stripe_log}/delete",
 *     "delete-multiple-form" = "/admin/content/stripe-log/delete-multiple",
 *   },
 *   field_ui_base_route = "entity.stripe_log.settings",
 * )
 */
final class StripeLog extends ContentEntityBase implements StripeLogInterface {

  use EntityOwnerTrait;

  /**
   * {@inheritdoc}
   */
  public function preSave(EntityStorageInterface $storage): void {
    parent::preSave($storage);
    if (!$this->getOwnerId()) {
      // If no owner has been set explicitly, make the anonymous user the owner.
      $this->setOwnerId(0);
    }
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type): array {

    $fields = parent::baseFieldDefinitions($entity_type);

    $fields['uid'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Author'))
      ->setSetting('target_type', 'user')
      ->setDefaultValueCallback(self::class . '::getDefaultEntityOwner')
      ->setDisplayOptions('form', [
        'type' => 'entity_reference_autocomplete',
        'settings' => [
          'match_operator' => 'CONTAINS',
          'size' => 60,
          'placeholder' => '',
        ],
        'weight' => 15,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'author',
        'weight' => 15,
      ])
      ->setDisplayConfigurable('view', TRUE);

    $fields['data'] = BaseFieldDefinition::create('string_long')
      ->setLabel(t('Stripe webhook data'))
      ->setDisplayOptions('view', [
        'type' => 'string',
        'weight' => 101,
        'label' => 'above',
      ])
      ->setDisplayConfigurable('view', TRUE)
      ->setDescription(t('The request data from stripe'));

    $fields['transaction'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Transaction'))
      ->setSetting('target_type', 'transaction')
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'author',
        'weight' => 15,
      ])
      ->setDisplayConfigurable('view', TRUE);

    $fields['event_id'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Stripe ID'))
      ->setRequired(TRUE)
      ->setTranslatable(TRUE)
      ->setRevisionable(TRUE)
      ->setDefaultValue('')
      ->setSetting('max_length', 255)
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => -5,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Authored on'))
      ->setDescription(t('The time that the stripe log was created.'))
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'timestamp',
        'weight' => 20,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('form', [
        'type' => 'datetime_timestamp',
        'weight' => 20,
      ])
      ->setDisplayConfigurable('view', TRUE);

    return $fields;
  }

}
