<?php

declare(strict_types=1);

namespace Drupal\starshot_stripe\Form;

use Drupal\Component\Utility\NestedArray;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Routing\TrustedRedirectResponse;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\Core\Url;
use Drupal\starshot_core\DevpanelApiInterface;
use Stripe\StripeClient;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

/**
 * Provides a Starshot_stripe form.
 */
final class PaymentForm extends FormBase {

  /**
   * The config factory to load config from.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;
  /**
   * The entity type manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The request stack.
   *
   * @var \Symfony\Component\HttpFoundation\RequestStack
   */
  protected $requestStack;

  /**
   * The current user.
   *
   * @var \Drupal\Core\Session\AccountProxyInterface
   */
  protected $currentUser;

  /**
   * The stripe client.
   *
   * @var \Stripe\StripeClient
   */
  protected $stripe;

  /**
   * The devpanel api service.
   *
   * @var \Drupal\starshot_core\DevpanelApiInterface
   */
  protected $devpanelApi;
  /**
   * The session service.
   *
   * @var \Symfony\Component\HttpFoundation\Session\SessionInterface
   */
  protected $session;

  /**
   * Contruct payment controler.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The factory for configuration objects.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager service.
   * @param \Symfony\Component\HttpFoundation\RequestStack $request_stack
   *   The request stack.
   * @param \Drupal\starshot_core\DevpanelApiInterface $devpanel_api
   *   The devpanel api service.
   * @param \Drupal\Core\Session\AccountProxyInterface $current_user
   *   The current user.
   * @param \Symfony\Component\HttpFoundation\Session\SessionInterface $session
   *   The session service.
   */
  public function __construct(
    ConfigFactoryInterface $config_factory,
    EntityTypeManagerInterface $entity_type_manager,
    RequestStack $request_stack,
    DevpanelApiInterface $devpanel_api,
    AccountProxyInterface $current_user,
    SessionInterface $session,
  ) {
    $this->configFactory = $config_factory;
    $this->entityTypeManager = $entity_type_manager;
    $this->requestStack = $request_stack;
    $config = $this->configFactory->get('stripe.settings');
    $secretKey = $config->get('apikey.' . $config->get('environment') . '.secret');
    $this->stripe = new StripeClient($secretKey);
    $this->devpanelApi = $devpanel_api;
    $this->currentUser = $current_user;
    $this->session = $session;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('entity_type.manager'),
      $container->get('request_stack'),
      $container->get('starshot_core.devpanel_api'),
      $container->get('current_user'),
      $container->get('session'),
    );
  }

  /**
   * Returns a page title.
   */
  public function getRenderTitle() {
    return "Subscribe";
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return 'starshot_stripe_payment';
  }

  /**
   * Get customerID.
   */
  public function getCustomerId() {
    $email = $this->currentUser->getEmail();
    $user = $this->entityTypeManager->getStorage('user')->load($this->currentUser()->id());
    $customerId = $user->field_stripe_customer_id->value ?? '';
    if (empty($customerId)) {
      $customer = $this->stripe->customers->create([
        'email' => $email,
      ]);
      $customerId = $customer->id ?? NULL;
      $user->set('field_stripe_customer_id', $customerId);
      $user->save();
    }
    return $customerId;
  }

  /**
   * Show all subscription on stripe.
   */
  public function showAllSubsription() {
    $customerId = $this->getCustomerId();
    if ($customerId) {
      $configuration = $this->stripe->billingPortal->configurations->create([
        'features' => [
          'customer_update' => [
            'allowed_updates' => ['email', 'tax_id'],
            'enabled' => TRUE,
          ],
          'invoice_history' => [
            'enabled' => TRUE,
          ],
          'payment_method_update' => [
            'enabled' => TRUE,
          ],
          'subscription_cancel' => [
            'enabled' => TRUE,
            'mode' => 'at_period_end',
          ],
        ],
      ]);

      $portal = $this->stripe->billingPortal->sessions->create([
        'customer' => $customerId,
        'return_url' => Url::fromUserInput('/user', ['absolute' => TRUE])->toString(),
        'configuration' => $configuration->id,
      ]);

      $response = new TrustedRedirectResponse($portal->url, 302);
      $response->send();
      exit;
    }
    else {
      $form['notice'] = [
        '#markup' => '
          <div class="checkout-notification">
          There no customer with email ' . $email . '<br>
          </div>',
      ];
      return $form;
    }
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, $app_id = NULL): array {
    if ($app_id == 'all') {
      $form = $this->showAllSubsription();
      return $form;
    }
    $values = $form_state->getValues();
    $wrapper_id = 'stripe-payment-form';
    $config = $this->configFactory->get('stripe.settings');
    $environment = $config->get('environment');
    $email = $this->currentUser->getEmail();
    $form = [
      '#prefix' => '<div id="' . $wrapper_id . '">',
      '#suffix' => '</div>',
    ];

    $form['#attached']['library'][] = 'starshot_stripe/payment';
    $form['#application'] = $this->devpanelApi->getApplicationById($app_id);
    if (empty($form['#application']->_id)) {
      throw new AccessDeniedHttpException();
    }

    $error = $this->devpanelApi->getError();
    if (empty($form['#application'])) {
      $form['notice'] = [
        '#markup' => '
          <div class="checkout-notification">
          ' . $error . '<br>
          </div>',
      ];
      return $form;
    }

    if ($form['#application']->status == "UPGRADING") {
      $form['notice'] = [
        '#markup' => '
          <div class="checkout-notification">
            Your application is upgrading. Please come back later<br>
          </div>',
      ];
      return $form;
    }

    // If have any payment then do not payment more.
    $storage = $this->entityTypeManager->getStorage('transaction');
    $existTransactions = $storage->getQuery()
      ->accessCheck(FALSE)
      ->condition('subscription_status', 'active')
      ->condition('app_id', $app_id)
      ->range(0, 1)
      ->execute();
    if ($existTransactions) {
      $transaction_id = reset($existTransactions);
      $transaction = $this->entityTypeManager->getStorage('transaction')->load($transaction_id);
      $sid = $transaction->subscription_id->value ?? '';
      $subscription = $this->stripe->subscriptions->retrieve($sid, []);
      $customerId = $subscription->customer ?? NULL;
      $return_url = Url::fromUserInput('/user/' . $this->currentUser->id() . '?time=' . time(), ['absolute' => TRUE])->toString();
      if ($customerId) {
        try {
          $products = $this->stripe->products->search([
            "query" => "active:'true' AND metadata['app_id']:'" . $app_id . "'",
          ]);
          $product_data = [];
          foreach ($products->data as $product) {
            $prices = $this->stripe->prices->all([
              'product' => $product->id,
              'active' => TRUE,
            ]);

            $price_ids = [];
            foreach ($prices->data as $price) {
              $price_ids[] = $price->id;
            }
            if (!empty($price_ids)) {
              $product_data[] = [
                'product' => $product->id,
                'prices' => $price_ids,
              ];
            }
          }

          if ($product_data) {
            $configuration = $this->stripe->billingPortal->configurations->create([
              'features' => [
                'customer_update' => [
                  'allowed_updates' => ['email', 'tax_id'],
                  'enabled' => TRUE,
                ],
                'invoice_history' => [
                  'enabled' => TRUE,
                ],
                'payment_method_update' => [
                  'enabled' => TRUE,
                ],
                'subscription_cancel' => [
                  'enabled' => TRUE,
                  'mode' => 'at_period_end',
                ],
                'subscription_update' => [
                  'enabled' => TRUE,
                  'default_allowed_updates' => ['price'],
                  'products' => $product_data,
                  'proration_behavior' => 'always_invoice',
                ],
              ],
            ]);

            $portal_update = $this->stripe->billingPortal->sessions->create([
              'customer' => $customerId,
              'return_url' => $return_url,
              'configuration' => $configuration->id,
              'flow_data' => [
                'type' => 'subscription_update',
                'subscription_update' => ['subscription' => $sid],
                'after_completion' => [
                  'type' => 'redirect',
                  'redirect' => ['return_url' => $return_url],
                ],
              ],
            ]);

            $response = new TrustedRedirectResponse($portal_update->url, 302);
            $response->send();
            exit;
          }
        }
        catch (Exception $e) {

          return $form['notice'] = [
            '#markup' => '
                 <div class="checkout-notification">
                   Your customer portal has not been setup. Please contact the us to get more detail<br>
                 </div>',
          ];
        }
      }
      else {
        return $form['notice'] = [
          '#markup' => '
               <div class="checkout-notification">
                 You have already made a payment. However, we are unable to match your customer portal with your email ' . $email . '<br>
               </div>',
        ];
      }
    }
    $form['period'] = [
      '#type' => 'radios',
      '#title' => $this->t('Your subscription period'),
      '#options' => [
        // 'day' => $this->t('Daily'),
        'month' => $this->t('Monthly'),
        'year' => $this->t('Yearly'),
      ],
      '#default_value' => 'month',
      '#ajax' => [
        'callback' => [get_called_class(), 'ajaxRefresh'],
        'wrapper' => $wrapper_id,
      ],
    ];
    $form['#period'] = $values['period'] ?? $form['period']['#default_value'];

    // Remove the test option.
    if ($environment != 'test') {
      unset($form['period']['#options']['day']);
    }

    $options = $this->getSubscriptionOptions($form, $form_state);
    $form['subscription'] = [
      '#type' => 'radios',
      '#title' => $this->t('Your application size'),
      '#options' => $options,
      '#ajax' => [
        'callback' => [get_called_class(), 'ajaxRefresh'],
        'wrapper' => $wrapper_id,
      ],
    ];

    if (!empty($values['period']) && !empty($values['subscription'])) {
      $subscription = $this->entityTypeManager->getStorage('node')->load($values['subscription']);
      $containter_size = $subscription->field_container_size->value ?? '';
      $amount = $subscription->field_price->value ?? '';

      $form['#amount'] = $amount;
      $form['#container_size'] = $containter_size;
      $form['#period'] = $values['period'];
      $form['#subscription'] = $values['subscription'];
      $form['actions'] = [
        '#type' => 'actions',
        'submit' => [
          '#type' => 'submit',
          '#disable' => TRUE,
          '#value' => $this->t('Continue'),
        ],
      ];
    }

    return $form;
  }

  /**
   * Ajax callback.
   */
  public static function ajaxRefresh(array $form, FormStateInterface $form_state) {
    $triggering_element = $form_state->getTriggeringElement();
    return NestedArray::getValue($form, array_slice($triggering_element['#array_parents'], 0, -3));
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $application = $form['#application'];
    $app_id = $application->_id;
    $last_submit = $this->session->get('payment_form_submit_' . $app_id, 0);
    // The form should not be submit to fast (after 60 second)
    // for product to creating.
    // See https://docs.stripe.com/api/products/search
    if (!empty($last_submit) && $last_submit > (time() - 60)) {
      $form_state->setError($form['subscription'], $this->t('Payment is processing. Please comeback after 1 minutes'));
    }

  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    $application = $form['#application'];
    $app_id = $form['#application']->_id;
    $active_key = $app_id . '_' . $form['#period'] . '_' . $form['#container_size'];
    $this->session->set('payment_form_submit_' . $app_id, time());
    $config_sizes = [
      'small_1',
      'medium_1',
      'large_1',
    ];

    foreach ($config_sizes as $config_size) {
      $products = $this->stripe->products->search([
        "query" => "active:'true' AND metadata['app_id']:'" . $app_id . "'  AND metadata['container_size']:'" . $config_size . "' ",
      ]);
      $product = $products->data[0] ?? NULL;
      if (!$product) {
        $product = $this->stripe->products->create([
          'name' => $this->t('@name - ID @id - @size', [
            '@name' => $application->project->name ?? '',
            '@id' => $application->_id ?? '',
            '@size' => $config_size,
          ]),
          'metadata' => [
            'app_id' => $app_id,
            'container_size' => $config_size,
          ],
        ]);
      }
      else {
        $this->stripe->products->update($product->id, [
          'name' => $this->t('@name - ID @id - @size', [
            '@name' => $application->project->name ?? '',
            '@id' => $application->_id ?? '',
            '@size' => $config_size,
          ]),
        ]);
      }

      $subscriptions = $this->entityTypeManager->getStorage('node')->loadByProperties([
        'type' => 'subscription',
        'field_container_size' => $config_size,
      ]);

      $prices = $this->stripe->prices->all([
        'product' => $product->id,
        'active' => TRUE,
      ]);
      foreach ($subscriptions as $subscription) {
        $period_type = $subscription->field_period_type->value;
        $size = $subscription->field_container_size->value;
        $amount = $subscription->field_price->value;
        $lookup_key = $app_id . '_' . $period_type . '_' . $size;
        $exist = FALSE;
        foreach ($prices->data as $price) {
          if (isset($price->metadata->lookup_key) && $price->metadata->lookup_key == $lookup_key) {
            $exist = TRUE;
            break;
          }
        }
        if (!$exist) {
          $price = $this->stripe->prices->create([
            'currency' => 'usd',
            'unit_amount' => $amount * 100,
            'recurring' => ['interval' => $period_type],
            'product' => $product->id,
            'nickname' => $this->t('@periodly for @size', [
              '@size' => $size,
              '@period' => $period_type != 'day' ? $period_type : 'dai',
            ]),
            'active' => TRUE,
            'metadata' => [
              'lookup_key' => $lookup_key,
              'container_size' => $size,
            ],
          ]);
        }
        if (isset($price->metadata->lookup_key) && $price->metadata->lookup_key == $active_key) {
          $active_price = $price;
        }
      }
    }

    $userPage = Url::fromRoute('entity.user.canonical', [
      'user' => $this->currentUser->id(),
    ],
    [
      'absolute' => TRUE,
    ])->toString();

    $customerId = $this->getCustomerId();
    $checkout_params = [
      'line_items' => [
        [
          'price' => $active_price->id,
          'quantity' => 1,
        ],
      ],
      'subscription_data' => [
        'metadata' => [
          'app_id' => $app_id,
          'user_id' => $this->currentUser->id(),
        ],
      ],
      'customer' => $customerId,
      'mode' => 'subscription',
      'success_url' => $userPage,
      'cancel_url' => $userPage,
    ];

    $checkout_session = $this->stripe->checkout->sessions->create($checkout_params);
    $response = new TrustedRedirectResponse($checkout_session->url, 301);
    $response->send();
  }

  /**
   * Generate Subscription Options.
   */
  protected function getSubscriptionOptions($form, $form_state) {
    $period_type = $form_state->getValue('period') ?? $form['period']['#default_value'];
    $subscriptions = $this->entityTypeManager->getStorage('node')->loadByProperties([
      'type' => 'subscription',
      'field_period_type' => $period_type,
    ]);
    $options = [];
    if ($subscriptions) {
      foreach ($subscriptions as $subscription) {
        $container_size = $subscription->field_container_size->value ?? '';
        $prize = $subscription->field_price->value ?? 0;
        $options[$subscription->id()] = $container_size . '/$' . $prize . ' ' . $this->t('per @type', ['@type' => $period_type]);
      }
    }
    return $options;
  }

}
