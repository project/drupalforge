<?php

declare(strict_types=1);

namespace Drupal\starshot_stripe\Form;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\ConfirmFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

/**
 * The Cancel Subscription Form .
 */
final class CancelSubscriptionForm extends ConfirmFormBase {

  /**
   * The transaction to delete.
   *
   * @var \Drupal\starshot_stripe\Entity\Transaction
   */
  protected $transaction;

  /**
   * The current user.
   *
   * @var \Drupal\Core\Session\AccountProxyInterface
   */
  protected $currentUser;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Construct CancelSubsriptionForm.
   *
   * @param \Drupal\Core\Session\AccountProxyInterface $current_user
   *   The current user.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   */
  public function __construct(
    AccountProxyInterface $current_user,
    EntityTypeManagerInterface $entity_type_manager,
  ) {
    $this->currentUser = $current_user;
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('current_user'),
      $container->get('entity_type.manager'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return 'starshot_stripe_cancel_subsription';
  }

  /**
   * {@inheritdoc}
   */
  public function getQuestion(): TranslatableMarkup {
    return $this->t('Are you sure you want to cancel subscription?');
  }

  /**
   * {@inheritdoc}
   */
  public function getCancelUrl() {
    $url = Url::fromRoute('entity.user.canonical', ['user' => $this->currentUser->id()]);
    return $url;
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, $transaction_id = '') {
    $this->transaction = $this->entityTypeManager->getStorage('transaction')->load($transaction_id);
    $status = $this->transaction->subscription_status->value ?? '';
    if (!$this->transaction || $this->transaction->getOwnerId() != $this->currentUser->id() || $status == 'canceled') {
      throw new AccessDeniedHttpException();
    }
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void {
    $this->transaction->set('subscription_status', 'canceled');
    $this->transaction->save();
    $form_state->setRedirectUrl(Url::fromRoute('entity.user.canonical', ['user' => $this->currentUser->id()]));
  }

}
