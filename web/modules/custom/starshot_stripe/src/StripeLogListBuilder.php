<?php

declare(strict_types=1);

namespace Drupal\starshot_stripe;

use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityListBuilder;

/**
 * Provides a list controller for the stripe log entity type.
 */
final class StripeLogListBuilder extends EntityListBuilder {

  /**
   * {@inheritdoc}
   */
  public function buildHeader(): array {
    $header['id'] = $this->t('ID');
    $header['transaction'] = $this->t('Transaction');
    $header['transaction_status'] = $this->t('Transaction Status');
    $header['event'] = $this->t('Event');
    $header['start'] = $this->t('Start date');
    $header['end'] = $this->t('End date');
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity): array {
    /** @var \Drupal\starshot_stripe\StripeLogInterface $entity */
    $row['id'] = $entity->toLink();
    $row['transaction']['data'] = $entity->get('transaction')->view(['label' => 'hidden']);
    $row['transaction_status']['data'] = $entity->transaction->entity->payment_status->value ?? '';

    $webhook = $entity->get('data')->value ?? '';
    $webhook = json_decode($webhook);
    $row['event']['data'] = $webhook->type ?? '';
    $row['start']['data'] = $webhook->data->object->current_period_start ?? '';
    $row['start']['data'] = DrupalDateTime::createFromTimestamp($row['start']['data'])->format('Y-m-d H:i:s');
    $row['end']['data'] = $webhook->data->object->current_period_end ?? '';
    $row['end']['data'] = DrupalDateTime::createFromTimestamp($row['end']['data'])->format('Y-m-d H:i:s');
    return $row + parent::buildRow($entity);
  }

}
