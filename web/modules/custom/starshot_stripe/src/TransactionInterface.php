<?php

declare(strict_types=1);

namespace Drupal\starshot_stripe;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface defining a transaction entity type.
 */
interface TransactionInterface extends ContentEntityInterface, EntityOwnerInterface {

}
