<?php

declare(strict_types=1);

namespace Drupal\starshot_stripe;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use GuzzleHttp\ClientInterface;
use Stripe\StripeClient;

/**
 * Stripe service.
 */
final class StripeService {

  /**
   * The config factory to load config from.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The HTTP Client service.
   *
   * @var \GuzzleHttp\ClientInterface
   */
  protected $httpClient;

  /**
   * The stripe client.
   *
   * @var \Stripe\StripeClient
   */
  protected $stripe;

  /**
   * Constructs a new StripeWebhookEventSubscriber object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The factory for configuration objects.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \GuzzleHttp\ClientInterface $http_client
   *   The client used to submit to engines.
   */
  public function __construct(
    ConfigFactoryInterface $config_factory,
    EntityTypeManagerInterface $entity_type_manager,
    ClientInterface $http_client,
  ) {
    $this->configFactory = $config_factory;
    $this->entityTypeManager = $entity_type_manager;
    $this->httpClient = $http_client;
    $config = $this->configFactory->get('stripe.settings');
    $secretKey = $config->get('apikey.' . $config->get('environment') . '.secret');
    $this->stripe = new StripeClient($secretKey);

  }

  /**
   * Extend the application.
   */
  public function extendApplication($app_id, $container_size, $subscription_ended) {
    $endpoint = $this->configFactory->get('starshot_core.settings')->get('apiEndpoint');
    $endpoint .= '/api/v2/drupal-forge/application/extend';
    $accessToken = $this->configFactory->get('starshot_core.settings')->get('apiToken');
    try {
      $this->httpClient->request('POST', $endpoint, [
        'body' => json_encode([
          'applicationId' => $app_id,
          'subscriptionEnded' => $subscription_ended,
          'capacity' => $container_size,
        ]),
        'headers' => [
          'Authorization' => $accessToken,
          'Content-Type' => 'application/json',
        ],
      ]);
    }
    catch (ServerException $e) {
      die("Can't connect to devpanel");
    }
    catch (Exception $e) {
      die($e->getMessage());
    }
  }

}
