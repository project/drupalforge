<?php

declare(strict_types=1);

namespace Drupal\starshot_stripe;

use Drupal\Core\Entity\ContentEntityInterface;

/**
 * Provides an interface defining a stripe log entity type.
 */
interface StripeLogInterface extends ContentEntityInterface {

}
