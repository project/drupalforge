<?php

namespace Drupal\starshot_stripe\EventSubscriber;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\starshot_core\DevpanelApiInterface;
use Drupal\starshot_stripe\Entity\StripeLog;
use Drupal\starshot_stripe\Entity\Transaction;
use Drupal\starshot_stripe\StripeService;
use Drupal\stripe\Event\StripeEvents;
use Drupal\stripe\Event\StripeWebhookEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Class EntityTypeSubscriber.
 *
 * @package Drupal\stripe_examples\EventSubscriber
 */
class StripeWebhookEventSubscriber implements EventSubscriberInterface {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;


  /**
   * The stripe service.
   *
   * @var \Drupal\starshot_stripe\StripeService
   */
  protected $stripeService;

  /**
   * The devpanel api service.
   *
   * @var \Drupal\starshot_core\DevpanelApiInterface
   */
  protected $devpanelApi;

  /**
   * Constructs a new StripeWebhookEventSubscriber object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\starshot_stripe\StripeService $stripe_service
   *   The stripe service.
   * @param \Drupal\starshot_core\DevpanelApiInterface $devpanel_api
   *   The devpanel api service.
   */
  public function __construct(
    EntityTypeManagerInterface $entity_type_manager,
    StripeService $stripe_service,
    DevpanelApiInterface $devpanel_api,
  ) {
    $this->entityTypeManager = $entity_type_manager;
    $this->stripeService = $stripe_service;
    $this->devpanelApi = $devpanel_api;

  }

  /**
   * {@inheritdoc}
   *
   * @return array
   *   The event names to listen for, and the methods that should be executed.
   */
  public static function getSubscribedEvents() {
    return [
      StripeEvents::WEBHOOK => 'webhook',
    ];
  }

  /**
   * React to a config object being saved.
   *
   * @param \Drupal\stripe\Event\StripePaymentEvent $event
   *   Stripe payment event.
   */
  public function webhook(StripeWebhookEvent $event) {
    $stripe_data = $event->getEvent();
    $events = [
      'customer.subscription.created',
      'customer.subscription.updated',
      'customer.subscription.deleted',
    ];

    if (in_array($stripe_data->type, $events)) {
      $app_id = $stripe_data['data']['object']['metadata']['app_id'] ?? '';
      $user_id = $stripe_data['data']['object']['metadata']['user_id'] ?? '';
      if (!empty($app_id)) {
        $subscription_id = $stripe_data['data']['object']['id'] ?? '';
        $event_id = $stripe_data->id ?? '';
        $stripe_logs = $this->entityTypeManager->getStorage('stripe_log')->loadByProperties(['event_id' => $event_id]);
        $cancled = FALSE;
        $transactions = $this->entityTypeManager->getStorage('transaction')->loadByProperties([
          'app_id' => $app_id,
          'subscription_id' => $subscription_id,
        ]);
        switch ($stripe_data->type) {
          case 'customer.subscription.created':
            if ($transactions) {
              $transaction = reset($transactions);
            }
            else {
              $transaction = Transaction::create([
                'app_id' => $app_id,
                'subscription_id' => $subscription_id,
                'uid' => $user_id,
                'currency' => 'USD',
              ]);
              $transaction->save();
            }
            $all_transactions = $this->entityTypeManager->getStorage('transaction')->loadByProperties([
              'app_id' => $app_id,
            ]);
            if ($all_transactions) {
              foreach ($all_transactions as $cancle) {
                // Ingnore current subscription.
                $sid = $cancle->subscription_id->value ?? NULL;
                if ($cancle->id() == $transaction->id() || empty($sid)) {
                  continue;
                }
                $cancle->set('subscription_status', 'canceled');
                $cancle->save();
              }
            }
            break;

          case 'customer.subscription.updated':
            $cancled = $stripe_data['data']['object']['cancel_at_period_end'] ?? FALSE;
            if (!$cancled) {
              if ($transactions) {
                $transaction = reset($transactions);
              }
            }
            else {
              if ($transactions) {
                $cancle = reset($transactions);
                $cancle->set('subscription_status', 'canceled');
                $cancle->save();
              }
            }
            break;

          case 'customer.subscription.deleted':
            if ($transactions) {
              $cancle = reset($transactions);
              $cancle->set('subscription_status', 'canceled');
              $cancle->save();
            }
            $cancled = TRUE;
            break;
        }

        if (isset($transaction)) {
          $amount = $stripe_data['data']['object']['plan']['amount'] ?? 0;
          $amount = $amount / 100;
          $container_size = $stripe_data['data']['object']['plan']['metadata']['container_size'] ?? '';
          $period = $stripe_data['data']['object']['plan']['interval'] ?? '';
          $subscription_ended = $stripe_data['data']['object']['current_period_end'] ?? time();
          $application = $this->devpanelApi->getApplicationById($app_id);
          $application_title = $application->project->name ?? '';
          $transaction->set('field_template_name', $application_title);
          $transaction->set('subscription_status', 'active');
          $transaction->set('amount', $amount);
          $transaction->set('container_size', $container_size);
          $transaction->set('period', $period);
          $transaction->set('payment_status', 'succeeded');
          $transaction->set('subscription_ended', $subscription_ended);
          $transaction->set('updated', time());
          $transaction->save();

          if (!$stripe_logs) {
            $stripe_log = StripeLog::create();
          }
          else {
            $stripe_log = reset($stripe_logs);
          }
          $stripe_log->set('event_id', $event_id);
          $stripe_log->set('transaction', $transaction->id());
          $stripe_log->set('data', json_encode($stripe_data));
          $stripe_log->save();
          $this->stripeService->extendApplication($app_id, $container_size, $subscription_ended);
          return $event;
        }
        elseif ($cancled) {
          die("Cancled subscription");
        }
        else {
          die("Can't find transaction");
        }

      }
      else {
        die("Can't find application");
      }
    }
    return $event;
  }

}
