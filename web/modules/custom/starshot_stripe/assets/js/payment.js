
(function (Drupal, once, $) {
  'use strict';

  Drupal.behaviors.stripePayment = {
    attach: function (context, settings) {
      // Add your JavaScript behavior here
      $(once('init', '.stripe-payment-form', context)).each(function() {
        let element = $(this);
        $('#period-tab .nav-link',element).click(function() {
          $('#period-tab .nav-item').removeClass('active');
          $(this).closest('.nav-item').addClass('active');
          $('#period-tab-content .tab-pane').removeClass('show active');
          $('#period-tab-content ' + $(this).attr('data-bs-target')).addClass('show active');
          let period = $(this).data('period');

          if ( $('input[name="period"]:checked').val() != period) {
            $('input[name="period"][value="' + period + '"]').prop('checked', true).trigger('change');
          }
       
        });
        $('#period-tab .nav-item.active .nav-link',element).trigger('click');
        $('.pricing-card .select-card',element).click(function(){
          $('.pricing-card').removeClass('active');
          $(this).closest('.pricing-card').addClass('active');
          let sid = $(this).data('sub');
          let radio = $('input[name="subscription"][value="' + sid + '"]');
          if (!radio.is(':checked')) {
            radio.prop('checked', true);
            setTimeout(function(){
              radio.trigger('change')
            },100)
          }
        });

        if(!$('.tab-pane.active .pricing-card.active').length) {
          $('.tab-pane.active .pricing-card:first-child .select-card').trigger('click');
        }
        
        // Prevent form is submit multiple time
        $('.form-submit',element).click(function() {
          $(this).prop('disabled', true);
          $(this).closest('form').trigger('submit');
        });
        
      });
    }
  };
})(Drupal, once, jQuery);
