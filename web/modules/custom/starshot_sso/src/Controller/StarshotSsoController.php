<?php

declare(strict_types=1);

namespace Drupal\starshot_sso\Controller;

use Aws\CognitoIdentityProvider\CognitoIdentityProviderClient;
use Drupal\Component\Utility\Random;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Routing\TrustedRedirectResponse;
use Drupal\Core\Url;
use Drupal\user\Entity\User;
use GuzzleHttp\ClientInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

/**
 * Returns responses for Starshot SSO routes.
 */
final class StarshotSsoController extends ControllerBase implements ContainerInjectionInterface {
  /**
   * The config factory to load config from.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;
  /**
   * The entity type manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The HTTP Client service.
   *
   * @var \GuzzleHttp\ClientInterface
   */
  protected $httpClient;
  /**
   * The request stack.
   *
   * @var \Symfony\Component\HttpFoundation\RequestStack
   */
  protected $requestStack;

  /**
   * The module handler service.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * Contruct payment controler.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The factory for configuration objects.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager service.
   * @param \GuzzleHttp\ClientInterface $http_client
   *   The client used to submit to engines.
   * @param \Symfony\Component\HttpFoundation\RequestStack $request_stack
   *   The request stack.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler service.
   */
  public function __construct(
    ConfigFactoryInterface $config_factory,
    EntityTypeManagerInterface $entity_type_manager,
    ClientInterface $http_client,
    RequestStack $request_stack,
    ModuleHandlerInterface $module_handler,
  ) {
    $this->configFactory = $config_factory;
    $this->entityTypeManager = $entity_type_manager;
    $this->httpClient = $http_client;
    $this->requestStack = $request_stack;
    $this->moduleHandler = $module_handler;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('entity_type.manager'),
      $container->get('http_client'),
      $container->get('request_stack'),
      $container->get('module_handler'),
    );
  }

  /**
   * Builds the response.
   */
  public function ssoCallback() {
    $cognito_client_id = $this->configFactory->get('starshot_sso.settings')->get('clientId');
    $cognito_client_secret = $this->configFactory->get('starshot_sso.settings')->get('clientSecret');
    $devpanelEndpoint = $this->configFactory->get('starshot_core.settings')->get('apiEndpoint');

    $data = [
      'grant_type' => 'authorization_code',
      'code' => $this->requestStack->getCurrentRequest()->query->get('code', '') ,
      'client_id' => $cognito_client_id,
      'redirect_uri' => Url::fromRoute('<current>')->setAbsolute()->toString(),
    ];
    $endpoint = 'https://login.site.devpanel.com/oauth2/token';
    try {
      $response = $this->httpClient->request('POST', $endpoint, [
        'form_params' => $data,
        'headers' => [
          'Content-Type' => 'application/x-www-form-urlencoded',
        ],

      ]);
      $response = $response->getBody()->getContents();
      $response = json_decode($response);
    }
    catch (\Exception $e) {
      $error_message = $e->getMessage();
      $this->messenger()->addMessage($error_message, 'error');
      throw new AccessDeniedHttpException();
    }

    $access_token = $response->access_token ?? '';
    if (empty($access_token)) {
      return $this->redirect('<front>');
    }

    // Create a CognitoIdentityProviderClient.
    $client = new CognitoIdentityProviderClient([
      'region' => 'us-west-1',
      'credentials' => [
        'key' => $cognito_client_id,
        'secret' => $cognito_client_secret,
      ],
    ]);
    try {
      $result = $client->getUser([
        'AccessToken' => $access_token,
      ]);
    }
    catch (\AwsException $e) {
      $error_message = $e->getAwsErrorMessage();
      $this->messenger()->addMessage($error_message, 'error');
      throw new AccessDeniedHttpException();
    }

    $random = new Random();
    $user_email = '';
    $user_password = $random->string(16);
    if (!empty($result['UserAttributes'])) {
      foreach ($result['UserAttributes'] as $attribute) {
        if ($attribute['Name'] == 'email') {
          $user_email = $attribute['Value'];
        }
      }
    }
    if (empty($user_email)) {
      $this->messenger()->addMessage($this->t("Can't find your email"), 'error');
      throw new AccessDeniedHttpException();
    }
    // Check if the user already exists.
    $user = user_load_by_mail($user_email);
    if (!$user) {
      $user = User::create([
        'name' => $user_email,
        'mail' => $user_email,
        'pass' => $user_password,
        'status' => 1,
        'roles' => ['authenticated'],
      ]);
      $user->save();
    }
    // Log in the newly created user.
    user_login_finalize($user);

    $redirect_url = $this->requestStack->getCurrentRequest()->cookies->get('redirect_url_after_login', $this->requestStack->getCurrentRequest()->getSchemeAndHttpHost());
    $redirectUrlVariables = ['redirectUrl' => $redirect_url];
    // Allow the redirectUrl to be altered via a custom hook.
    $this->moduleHandler->alter('sso_redirect_url', $redirectUrlVariables);

    $params = [
      'query' => [
        'accessToken' => $response->access_token ?? '',
        'idToken' => $response->id_token ?? '',
        'redirectUrl' => $redirectUrlVariables['redirectUrl'],
      ],
    ];

    $url = URL::fromUri($devpanelEndpoint . '/integrate-sso', $params)->toString();
    $response = new TrustedRedirectResponse($url);
    $response->addCacheableDependency($url);
    return $response;
  }

}
