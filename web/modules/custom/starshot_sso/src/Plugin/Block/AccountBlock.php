<?php

declare(strict_types=1);

namespace Drupal\starshot_sso\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides an account block block.
 *
 * @Block(
 *   id = "starshot_sso_account_block",
 *   admin_label = @Translation("Account block"),
 *   category = @Translation("Custom"),
 * )
 */
final class AccountBlock extends BlockBase implements ContainerFactoryPluginInterface {
  /**
   * The form builder.
   *
   * @var \Drupal\Core\Form\FormBuilderInterface
   */
  protected $formBuilder;
  /**
   * The current user.
   *
   * @var \Drupal\Core\Session\AccountProxyInterface
   */
  protected $currentUser;
  /**
   * The entity type manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    $instance = new static($configuration, $plugin_id, $plugin_definition);
    $instance->formBuilder = $container->get('form_builder');
    $instance->currentUser = $container->get('current_user');
    $instance->entityTypeManager = $container->get('entity_type.manager');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function build(): array {
    if ($this->currentUser->isAuthenticated()) {
      $user_entity = $this->entityTypeManager->getStorage('user')->load($this->currentUser->id());
      $build['#theme'] = 'starshot_sso_account';
      $build['#account'] = [
        'email' => $user_entity->getEmail(),
        'logout' => Url::fromRoute('user.logout')->toString(),
        'link' => Url::fromRoute('entity.user.canonical', ['user' => $this->currentUser->id()]),
      ];
      $space = $user_entity->field_space->entity ?? NULL;
      if ($space) {
        $build['#account']['space'] = $space->toUrl();
      }
    }
    else {
      $build = $this->formBuilder->getForm('Drupal\starshot_sso\Form\AccountForm');
    }
    $build['#cache']['contexts'][] = 'user';
    return $build;

  }

}
