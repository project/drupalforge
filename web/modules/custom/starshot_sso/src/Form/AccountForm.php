<?php

declare(strict_types=1);

namespace Drupal\starshot_sso\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Routing\TrustedRedirectResponse;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Cookie;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Provides a Starshot SSO form.
 */
final class AccountForm extends FormBase {

  /**
   * The request stack.
   *
   * @var \Symfony\Component\HttpFoundation\RequestStack
   */
  protected $requestStack;

  /**
   * Constructs a new AccountForm.
   *
   * @param \Symfony\Component\HttpFoundation\RequestStack $request_stack
   *   The request stack.
   */
  public function __construct(RequestStack $request_stack) {
    $this->requestStack = $request_stack;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('request_stack')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return 'starshot_sso_login';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state): array {
    $form['actions'] = [
      '#type' => 'actions',
      'submit' => [
        '#type' => 'submit',
        '#value' => $this->t('Login'),
      ],
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state): void {
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void {
    $current_host = $this->requestStack->getCurrentRequest()->getSchemeAndHttpHost();
    $redirect_url_after_login = $this->requestStack->getCurrentRequest()->getUri();
    $cookie = Cookie::create('redirect_url_after_login')
      ->withValue($redirect_url_after_login)
      ->withExpires(time() + 3600)
      ->withPath('/');

    $url = 'https://login.site.devpanel.com/login?redirect_uri=' . $current_host . '/callback&response_type=code&client_id=6vs705tbkogckspnlo5iq3o00s&identity_provider=COGNITO&scope=phone%20email%20openid%20profile%20aws.cognito.signin.user.admin';
    $url = Url::fromUri($url, ['absolute' => TRUE]);
    $response = new TrustedRedirectResponse($url->toString());
    $response->headers->setCookie($cookie);
    $form_state->setResponse($response);
  }

}
