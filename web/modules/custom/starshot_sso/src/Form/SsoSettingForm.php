<?php

declare(strict_types=1);

namespace Drupal\starshot_sso\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Configure Startshot SSO settings for this site.
 */
final class SsoSettingForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return 'starshot_sso_setting';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames(): array {
    return ['starshot_sso.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state): array {
    $form['clientId'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Cognito Web clien ID'),
      '#default_value' => $this->config('starshot_sso.settings')->get('clientId'),
      '#required' => TRUE,
    ];
    $form['clientSecret'] = [
      '#type' => 'password',
      '#title' => $this->t('Cognito Web client secret'),
      '#placeholder' => $this->config('starshot_sso.settings')->get('clientSecret') ? str_repeat('●', 32) : '',
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void {
    $config = $this->config('starshot_sso.settings');

    $config->set('clientId', $form_state->getValue('clientId'));
    if ($form_state->getValue('clientSecret')) {
      $config->set('clientSecret', $form_state->getValue('clientSecret'));
    }
    $config->save();
    parent::submitForm($form, $form_state);
  }

}
