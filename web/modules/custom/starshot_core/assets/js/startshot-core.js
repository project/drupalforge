/**
 * @file
 * JavaScript file for the starshot core module.
 */

 (function (Drupal, once, $) {
  'use strict';

  var StarshotCore = {};

  StarshotCore.init = function(context) {
    // StarshotCore.countdown();
    StarshotCore.handlePassword();
    StarshotCore.context = context;
    StarshotCore.openLanchModal();
    StarshotCore.accordion();
    StarshotCore.applicationReady();
  }
  StarshotCore.handlePassword = function(context) {
    $(once('submission-view', '.submission-view',  StarshotCore.context)).each(function () {
    const element = document.querySelectorAll('.submission-view');
      if (!element.length) {
        return false;
      }
      const usernameElements = document.querySelectorAll('.submission-view .inline-username input');
      const passwordElements = document.querySelectorAll('.submission-view .inline-password input');
      function showPassword(passwordInput) {
        const showButton = passwordInput.parentElement.querySelector('.show-password');
        if (showButton) {
          showButton.addEventListener('click', function(event) {
            if (passwordInput.type == "text") {
              passwordInput.type = "password";
              passwordInput.parentElement.classList.remove("show");
            } else {
              passwordInput.parentElement.classList.add("show");
              passwordInput.type = "text";
            }
          });
        }
      }
      function addMessage(_message) {
        const message = document.querySelector('.inline-message');
        message.innerHTML = _message;
     
      }
      function copyUsername(usernameInput) {
        const copyButton = usernameInput.parentElement.querySelector('.copy-username');
        if (!copyButton) return false;
        copyButton.addEventListener('click', function(event) {
          usernameInput.type = "text";
          usernameInput.select();
          document.execCommand("copy");
          addMessage('User name copied to clipboard!');

        });
      }
      function copyPassword(passwordInput) {
        const copyButton = passwordInput.parentElement.querySelector('.copy-password');
        if (!copyButton) return false;
        copyButton.addEventListener('click', function(event) {
          passwordInput.type = "text";
          passwordInput.select();
          document.execCommand("copy");
          passwordInput.type = "password"; // change back to password type
          addMessage('Password copied to clipboard!');

        });
      }

      // Use forEach to iterate over the selected elements
      usernameElements.forEach((usernameInput, index) => {
        copyUsername(usernameInput);
      });
      passwordElements.forEach((passwordInput, index) => {
        showPassword(passwordInput);
        copyPassword(passwordInput);
      });
    });

  }

  StarshotCore.applicationReady = function() {
    $(once('waiting-app-ready', '.waiting-app-ready',  StarshotCore.context)).each(function () {
      let element = $(this);
      setInterval(function(){
        window.location.reload();
      },10000)
    });

  }
  StarshotCore.accordion = function () {
    let element = $('.accordion');
    if (!element.length) return false;

    element.accordion({
      collapsible: true,
      heightStyle: "content",
    });
  }
  StarshotCore.openLanchModal = function() {
    let element = $('#open-modal');
    if (!element.length) return false;

    element.on('click', function(e) {
      // Open a jQuery UI dialog with the specified options
      $('#modal-content').dialog({
        modal: true,
        title: $('#modal-content').data('title'),
        maxWidth: 900,
        width: 'auto',
        fluid: true, //new option
      });
    });
  }
  StarshotCore.countdown = function() {
    let element = $('.submission-view .sidebar .countdown');
    if (!element.length) return false;

    let targetDate = new Date(element.data('expired')).getTime();
    // Update the countdown every second
    let x = setInterval(function() {
      let now = new Date().getTime();
      let distance = targetDate - now;

      // Calculations for days, hours, minutes, and seconds
      let hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
      let minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
      let seconds = Math.floor((distance % (1000 * 60)) / 1000);
      hours = hours < 10 ? '0' +  hours : hours;
      minutes = minutes < 10 ? '0' +  minutes : minutes;
      seconds = seconds < 10 ? '0' +  seconds : seconds;
      // Display the countdown in the element with id "countdown"
      element.html( hours + ":" + minutes + ":" + seconds);

      let btn = element.closest('.cowndown-wrapper').find('._btn');
      btn.click(function(e){
        if ($(this).hasClass('disabled')) {
          e.preventDefault();
          return false;
        }
      });

      // If the countdown is over, display a message
      if (distance < 0) {
        clearInterval(x);
        element.addClass('expired');
        btn.removeClass('disabled');
        element.html("Application expired");
      }
    }, 1000);

  }


  Drupal.behaviors.StarshotCore = {
    attach(context) {
      StarshotCore.init(context);
    }
  }
})(Drupal, once, jQuery);
