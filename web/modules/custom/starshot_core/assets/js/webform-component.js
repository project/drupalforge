'use strict';

class WebformComponent extends HTMLElement {
  templateId = null;
  reference = null;
  setCookie(cname, cvalue, exhours) {
    const d = new Date();
    d.setTime(d.getTime() + exhours * 60 * 60 * 1000);
    let expires = 'expires=' + d.toUTCString();
    document.cookie = cname + '=' + cvalue + '; ' + expires + '; path=/';
  }
  getCookie(cname) {
    let name = cname + '=';
    let decodedCookie = decodeURIComponent(document.cookie);
    let ca = decodedCookie.split(';');
    for (let i = 0; i < ca.length; i++) {
      let c = ca[i];
      while (c.charAt(0) == ' ') {
        c = c.substring(1);
      }
      if (c.indexOf(name) == 0) {
        return c.substring(name.length, c.length);
      }
    }
    return false;
  }
  countDown() {
    const element = this;
    let countdownElement = element.querySelector('.countdown');
    let targetDate = countdownElement
      ? countdownElement.getAttribute('expired')
      : null;

    if (!countdownElement || !targetDate) return false;

    // Update the countdown every second
    let x = setInterval(function () {
      let now = new Date().getTime();
      let distance = targetDate - now;

      // Calculations for days, hours, minutes, and seconds
      let hours = Math.floor(
        (distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60)
      );
      let minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
      let seconds = Math.floor((distance % (1000 * 60)) / 1000);
      hours = hours < 0 ? 0 : hours;
      hours = hours < 10 ? '0' + hours : hours;
      minutes = minutes < 0 ? 0 : minutes;
      minutes = minutes < 10 ? '0' + minutes : minutes;
      seconds = seconds < 0 ? 0 : seconds;
      seconds = seconds < 10 ? '0' + seconds : seconds;
      // Display the countdown in the element with id 'countdown'
      countdownElement.innerHTML = hours + ':' + minutes + ':' + seconds;

      // If the countdown is over, display a message
      if (distance < 0) {
        clearInterval(x);
        countdownElement.innerHTML = 'Application expired';
      }
    }, 1000);
  }
  addStyle() {
    const style = document.createElement('style');
    style.innerHTML = `
        .webform-component {
          position: relative;
          max-width: 494px;
          width: 100%;
          height: 260px;
          display: block;
          margin: auto;
          box-shadow: 4px 4px 16px 12px rgba(85, 204, 255, 0.16);
          border-radius: 24px;
          overflow: hidden;
          background-color: #064771;
        }
        .webform-component.loaded {
          height: auto;
        }
        .webform-component:before {
          content:'';
          position:absolute;
          background: url(${this.domain}/modules/custom/starshot_core/assets/images/loading.gif) no-repeat center center/100%;
          background-color: #064771;
          display: block;
          width: 100%;
          height: 100%;
          z-index:-1;

          opacity: 0;
          transition: all .5s ease;
        }
        .webform-component.loading:before {
          opacity: 1;
          z-index:1;
        }
        .webform-component .message {
          color: #ffff;
          padding: 0px 20px;
          height: 100%;
          display: flex;
          align-items: center;
          width: 100%;
        }
      `;
    document.head.appendChild(style);
  }

  addLoading() {
    this.classList.add('loading');
  }

  removeLoading() {
    this.classList.remove('loading');
  }

  showError(message) {
    this.innerHTML = `
        <div class='message'>${message}</div>
    `;
  }

  addCaptcha() {
    const captcha = document.querySelector('.captcha__element');
    if (!captcha) {
      return false;
    }

    const script = document.createElement('script');
    script.src = 'https://www.google.com/recaptcha/api.js';
    script.async = true;
    script.defer = true;
    document.body.appendChild(script);
  }

  handleToggle () {
    const toggleDeveloper = document.getElementById('toggle-developer');
    const developerElement = document.querySelector('.drupal-available__for-developer');

    if (developerElement && toggleDeveloper) {
      developerElement.style.transition = 'opacity 0.1s ease';

      toggleDeveloper.addEventListener('change', function() {
        if (toggleDeveloper.checked) {
          developerElement.style.display = 'grid';
          developerElement.style.opacity = '1';
        } else {
          developerElement.style.opacity = '0';
          developerElement.style.display = 'none';
        }
      });

      if (toggleDeveloper.checked) {
        developerElement.style.display = 'grid';
        developerElement.style.opacity = '1';
      } else {
        developerElement.style.display = 'none';
        developerElement.style.opacity = '0';
      }
    }
  }

  handlePassword() {
    const element = this;
    const usernameElements = document.querySelectorAll(
      '.inline-username input'
    );
    const passwordElements = document.querySelectorAll(
      '.inline-password input'
    );
    function showPassword(passwordInput) {
      const showButton =
        passwordInput.parentElement.querySelector('.show-password');
      if (showButton) {
        showButton.addEventListener('click', function (event) {
          if (passwordInput.type == 'text') {
            passwordInput.type = 'password';
            passwordInput.parentElement.classList.remove('show');
          } else {
            passwordInput.parentElement.classList.add('show');
            passwordInput.type = 'text';
          }
        });
      }
    }

    function addMessage(_message) {
      const box = element.querySelector('.drupal-available');
      const message = box.querySelector('.inline-message');
      if (message) {
        message.remove();
      }
      var newHTML = '<p class="inline-message">' + _message + "</p>";
      box.insertAdjacentHTML('beforeend', newHTML);
      setTimeout(function () {
        const message = box.querySelector('.inline-message');
        message.remove();
      }, 3000);
    }

    function copyUsername(usernameInput) {
      const copyButton =
        usernameInput.parentElement.querySelector('.copy-username');
      if (!copyButton) return false;
      copyButton.addEventListener('click', function (event) {
        usernameInput.type = 'text';
        usernameInput.select();
        document.execCommand('copy');
        addMessage('User name copied to clipboard!');
      });
    }

    function copyPassword(passwordInput) {
      const copyButton =
        passwordInput.parentElement.querySelector('.copy-password');
      if (!copyButton) return false;
      copyButton.addEventListener('click', function (event) {
        passwordInput.type = 'text';
        passwordInput.select();
        document.execCommand('copy');
        passwordInput.type = 'password'; // change back to password type
        addMessage('Password copied to clipboard!');
      });
    }

    // Use forEach to iterate over the selected elements
    usernameElements.forEach((usernameInput, index) => {
      copyUsername(usernameInput);
    });
    passwordElements.forEach((passwordInput, index) => {
      showPassword(passwordInput);
      copyPassword(passwordInput);
    });
  }

  handleSaveApp() {
    const btnSave = document.getElementById('btn-save-app');
    if (btnSave) {
      btnSave.addEventListener('click', async function (e) {
        e.preventDefault();
        btnSave.setAttribute('disabled', true);
        const response = await fetch(btnSave.getAttribute('href'));
        btnSave.setAttribute('disabled', false);
        if (response.status === 401) {
          const responseJson = await response.json();
          window.location.href = responseJson.login_url;
        } else if (response.status === 200) {
          window.location.reload();
        }
      });
    }
  }

  correctResourceUrl(html) {
    html = html.replaceAll('"/sites/default','"'+this.domain +'/sites/default');
    html = html.replaceAll('"/core/','"'+this.domain +'/core/');
    html = html.replaceAll('"/themes/','"'+this.domain + '/themes/');
    html = html.replaceAll('"/modules/','"'+ this.domain + '/modules/');
    return html;
  }

  handleForm() {
    const element = this;
    element.handlePassword();
    element.waitingAppReady();
    element.countDown();
    element.addCaptcha();
    element.handleToggle();
    element.handleSaveApp();

    const form = document.getElementById(
      'webform-submission-starshot-quickstart-add-form'
    );
    if (!form) return false;

    form.addEventListener('submit', async function (event) {
      event.preventDefault();
      element.addLoading();
      const currentURL = window.location.href;
      const reference = document.querySelector('input[name="reference"]');
      reference.value = element.reference ?? currentURL;
      const formData = new FormData(this); // Create a FormData object from the form
      try {
        let formUrl = element.domain + '/form/starshot-quickstart?embed=1';
        if (element.templateId) {
          formUrl += '&template=' + element.templateId;
        }
        const response = await fetch(formUrl, {
          // Replace with your target URL
          method: 'POST',
          body: formData,
          redirect: 'follow',
        });

        if (response.ok) {
          let html = await response.text();
          html = element.correctResourceUrl(html);
          element.innerHTML = html;
          element.handleForm();
          setTimeout(function () {
            const available = element.querySelector('.drupal-available');
            const application = available.getAttribute('data-submission-id');
            const templateId = available.getAttribute('data-template-id');
            element.setCookie('template_' + templateId, application, 6);
          }, 1000);
        } else {
          element.showError('Form submission failed. Please try again later');
        }
      } catch (error) {
        element.showError('Form submission failed. Please try again later');
        console.error('Error:', error);
      }
      element.removeLoading();
    });
  }

  waitingAppReady() {
    const element = this;
    const waiting = document.querySelector('.waiting-app-ready');
    if (waiting) {
      setInterval(function () {
        // Check applicaton is ready by fetch to URL
        const apiUrl =
          element.domain + waiting.getAttribute('data-application-submission');
        fetch(apiUrl)
          .then((response) => {
            if (response.ok) {
              window.location.reload();
            }
          })
          .catch((error) => {
            console.error('Error:', error);
          });
      }, 10000);
    }
  }

  async connectedCallback() {
    const element = this;
    element.domain = element.getAttribute('host');
    element.templateId = element.getAttribute('templateId') ?? 71; //Drupal CMS ID In production
    element.reference = element.getAttribute('reference');
    element.classList.add('webform-component');
    element.addStyle();
    element.addLoading();
    let application = this.getCookie('template_' + element.templateId);

    let html = '';
    let newForm = true;
    if (application) {
      try {
        const response = await fetch(
          this.domain + '/submission/' + application + '?embed=1'
        );
        if (response.ok && response.status == 200) {
          html = await response.text();
          newForm = false;
        }
      } catch (error) {
        console.error('Error:', error);
      }
    }
    if (newForm) {
      let formUrl = this.domain + '/form/starshot-quickstart?embed=1';
      if (element.templateId) {
        formUrl += '&template=' + element.templateId;
      }
      const response = await fetch(formUrl);
      html = await response.text();
    }

    html = element.correctResourceUrl(html);
    element.innerHTML = html;
    element.handleForm();
    setTimeout(function () {
      element.removeLoading();
      element.classList.add('loaded');
    }, 3000);
  }
}
customElements.define('webform-component', WebformComponent);
