<?php

declare(strict_types=1);

namespace Drupal\starshot_core\Controller;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Session\AccountProxyInterface;
use GuzzleHttp\ClientInterface;
use GuzzleHttp\Exception\ServerException;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Cookie;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

/**
 * Returns responses for Starshot stripe routes.
 */
final class StarshotCoreController extends ControllerBase {
  /**
   * The config factory to load config from.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;
  /**
   * The entity type manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The HTTP Client service.
   *
   * @var \GuzzleHttp\ClientInterface
   */
  protected $httpClient;

  /**
   * The current user.
   *
   * @var \Drupal\Core\Session\AccountProxyInterface
   */
  protected $currentUser;

  /**
   * The entity type manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */

  /**
   * Contruct Startshot Core controler.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The factory for configuration objects.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager service.
   * @param \GuzzleHttp\ClientInterface $http_client
   *   The client used to submit to engines.
   * @param Drupal\Core\Session\AccountProxyInterface $current_user
   *   The current user.
   */
  public function __construct(
    ConfigFactoryInterface $config_factory,
    EntityTypeManagerInterface $entity_type_manager,
    ClientInterface $http_client,
    AccountProxyInterface $current_user,
  ) {
    $this->configFactory = $config_factory;
    $this->entityTypeManager = $entity_type_manager;
    $this->httpClient = $http_client;
    $this->currentUser = $current_user;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('entity_type.manager'),
      $container->get('http_client'),
      $container->get('current_user'),
    );
  }

  /**
   * Confirm the subscription.
   */
  public function submissionConfirm(Request $request, $submission_id = NULL) {
    $submissions = $this->entityTypeManager->getStorage('webform_submission')->loadByProperties(['uuid' => $submission_id]);
    if (!$submissions) {
      throw new AccessDeniedHttpException();
    }
    $submission = reset($submissions);
    $webform = $submission->getWebform();
    if ($webform->id() === 'subscription') {
      $submitted = $submission->getData();
      if (!empty($submitted['include_dev'])) {
        $response = new RedirectResponse('/next-steps-w-cde', 302);
        $response->send();
      }
      else {
        $response = new RedirectResponse('/next-steps', 302);
        $response->send();
      }
    }
    return [];
  }

  /**
   * View the submission.
   */
  public function submissionView(Request $request, $submission_id = NULL) {
    $submissions = $this->entityTypeManager->getStorage('webform_submission')->loadByProperties(['uuid' => $submission_id]);
    if (!$submissions) {
      throw new AccessDeniedHttpException();
    }
    $submission = reset($submissions);
    $submitted = $submission->getData();
    $submitted['template'] = $submitted['template'] ?? 0;
    $template = $this->entityTypeManager->getStorage('node')->load($submitted['template']);

    $endpoint = $this->configFactory->get('starshot_core.settings')->get('apiEndpoint');
    $endpoint .= '/api/v2/drupal-forge/application/view?submissionId=' . $submission->uuid();
    $accessToken = $this->configFactory->get('starshot_core.settings')->get('apiToken');
    if (!empty($submitted['reference'])) {
      $reference = $submitted['reference'];
      // Extract domain from URL using regex.
      preg_match('/^(?:https?:\/\/)?(?:[^@\n]+@)?(?:www\.)?([^:\/\n]+)/', $reference, $matches);
      // The domain will be stored in the first capture group ($matches[1])
      $domain = $matches[1] ?? NULL;
      if ($domain) {
        $domain = str_replace('drupaleasy.com', 'drupaleasy.app', $domain);
        $endpoint .= '&originUrl=' . $domain;
      }
    }

    try {
      $response = $this->httpClient->request('GET', $endpoint, [
        'headers' => [
          'Authorization' => $accessToken,
          'Content-Type' => 'application/json',
        ],
      ]);

      $response = $response->getBody()->getContents();
      $response = json_decode($response);

      // Uncomment to debug mode
      // $response = new \stdClass();
      // $response->isActive = true;.
      $startshot_available = $response;
      $startshot_available->templateId = $template->id();
      $startshot_available->templateTitle = $template->getTitle();
      $startshot_available->embedDescription = $template->get('field_embed_description')->value;
      $startshot_available->submission_id = $submission_id;
      $startshot_available->email = $submitted['email'] ?? '';
      $startshot_available->workspaceUrl = !empty($response->isIncludeDev) ? ($response->workspaceUrl ?? '') : '';
      // Do not show VS code for site builder.
      if ($submission->getWebform()->id() == 'trial') {
        $startshot_available->vscodeURL = '';
        $startshot_available->vscodePassword = '';
      }

      $route_parameters = $request->query->all();
      if (!empty($route_parameters['embed'])) {
        $build = [
          '#theme' => 'submission_embed',
          '#startshot_available' => $startshot_available,
        ];
        return $build;
      }
    }
    catch (ServerException $e) {
      $this->messenger()->addMessage($this->t("Can't connect to devpanel"), 'error');
    }
    catch (\Exception $e) {
      $error = $e->getResponse();
      $error = $error ? $error->getBody() : NULL;
      $error = $error ? $error->getContents() : NULL;
      $error = $error ? json_decode($error) : NULL;
      if (isset($error->message)) {
        $error_message = is_array($error->message) ? implode(', ', $error->message) : $error->message;
      }
      else {
        $error_message = $e->getMessage();
      }
      $this->messenger()->addMessage($error_message, 'error');
      throw new AccessDeniedHttpException();
    }
    if (empty($response)) {
      return [];
    }
    $build = [
      '#theme' => 'submission',
      '#startshot_available' => $startshot_available,
    ];

    $build['#attached']['library'][] = "starshot_core/starshot_core";
    return $build;
  }

  /**
   * Check the subscription status.
   */
  public function submissionStatus(Request $request, $submission_id = NULL) {
    $url = $request->query->get('applicationURL');
    try {
      $this->httpClient->request('GET', $url);
    }
    catch (\Exception $e) {
      return new JsonResponse($e->getMessage(), 404);
    }
    $output = [
      'message' => $this->t('Application ready'),
    ];
    return new JsonResponse($output);
  }

  /**
   * Save Application.
   */
  public function saveApplication(Request $request, $submission_id) {
    $user = $this->currentUser;
    if (!$user->isAuthenticated()) {
      $current_host = $request->getSchemeAndHttpHost();
      $referer = $request->headers->get('referer');
      $curent_request = Request::create($referer);
      $redirect_url_after_login = $request->getSchemeAndHttpHost() . $curent_request->getRequestUri();

      $cookie = Cookie::create('redirect_url_after_login')
        ->withValue($redirect_url_after_login)
        ->withExpires(time() + 3600)
        ->withPath('/');

      $url = 'https://login.site.devpanel.com/login?redirect_uri=' . $current_host . '/callback&response_type=code&client_id=6vs705tbkogckspnlo5iq3o00s&identity_provider=COGNITO&scope=phone%20email%20openid%20profile%20aws.cognito.signin.user.admin';
      $response = new JsonResponse(['login_url' => $url], Response::HTTP_UNAUTHORIZED);
      $response->headers->setCookie($cookie);
      return $response;
    }

    $endpoint = $this->configFactory->get('starshot_core.settings')->get('apiEndpoint');
    $endpoint .= '/api/v2/drupal-forge/application/save';
    $accessToken = $this->configFactory->get('starshot_core.settings')->get('apiToken');
    $data = [
      'email' => $user->getEmail(),
      'submissionId' => $submission_id,
    ];
    try {
      $response = $this->httpClient->request('POST', $endpoint, [
        'form_params' => $data,
        'headers' => [
          'Authorization' => $accessToken,
          'Content-Type' => 'application/x-www-form-urlencoded',
        ],
      ]);
      $response = $response->getBody()->getContents();
    }
    catch (ServerException $e) {
      return new JsonResponse($e->getMessage(), 400);
    }
    catch (\Exception $e) {
      return new JsonResponse($e->getMessage(), 500);
    }

    return new JsonResponse($response, Response::HTTP_OK);
  }

}
