<?php

namespace Drupal\starshot_core\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides an example block.
 *
 * @Block(
 *   id = "starshot_core_try_free_block",
 *   admin_label = @Translation("Try Free"),
 *   category = @Translation("Startshot core")
 * )
 */
class TryFreeBlock extends BlockBase implements ContainerFactoryPluginInterface {

  /**
   * The ModuleHandler service.
   *
   * @var \Drupal\Core\Module\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * The route match.
   *
   * @var \Drupal\Core\Routing\RouteMatchInterface
   */
  protected $routeMatch;

  /**
   * The related request stack.
   *
   * @var \Symfony\Component\HttpFoundation\RequestStack
   */
  protected $requestStack;

  /**
   * The entity type manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The config factory to load config from.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    $instance = new static($configuration, $plugin_id, $plugin_definition);
    $instance->moduleHandler = $container->get('module_handler');
    $instance->routeMatch = $container->get('current_route_match');
    $instance->entityTypeManager = $container->get('entity_type.manager');
    $instance->requestStack = $container->get('request_stack');
    $instance->configFactory = $container->get('config.factory');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    $node                            = $this->routeMatch->getParameter('node');
    $webform                         = $this->entityTypeManager->getStorage('webform')->load('trial');
    $build['#form']['form']          = $this->entityTypeManager->getViewBuilder('webform')->view($webform);
    $build['#theme']                 = 'trial_form';
    $build['#node_id']               = $node->id();
    $build['#cache']['contexts']     = ['url'];
    $build['#attached']['library'][] = 'core/drupal.dialog.ajax';
    $build['#attached']['library'][] = 'starshot_core/starshot_core';

    return $build;
  }

}
