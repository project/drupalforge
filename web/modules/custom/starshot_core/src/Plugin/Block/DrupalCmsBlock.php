<?php

declare(strict_types=1);

namespace Drupal\starshot_core\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\node\NodeInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Symfony\Component\HttpFoundation\RequestStack;
use Drupal\Core\Config\ConfigFactoryInterface;

/**
 * Provides a drupal cms block.
 *
 * @Block(
 *   id = "starshot_core_drupal_cms",
 *   admin_label = @Translation("Drupal CMS"),
 *   category = @Translation("Custom"),
 * )
 */
final class DrupalCmsBlock extends BlockBase implements ContainerFactoryPluginInterface {
  /**
   * The request stack.
   *
   * @var \Symfony\Component\HttpFoundation\RequestStack
   */
  protected RequestStack $requestStack;

  /**
   * The config factory to load config from.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected ConfigFactoryInterface $configFactory;

  /**
   * Entity interface.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected EntityTypeManagerInterface $entityTypeManager;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    $instance = new static($configuration, $plugin_id, $plugin_definition);
    $instance->requestStack = $container->get('request_stack');
    $instance->configFactory = $container->get('config.factory');
    $instance->entityTypeManager = $container->get('entity_type.manager');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration(): array {
    return [
      'body' => '',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state): array {
    $form['body'] = [
      '#type' => 'text_format',
      '#title' => $this->t('Description'),
      '#default_value' => '',
      '#format' => 'full_html',
      '#default_value' => $this->configuration['body']['value'] ?? '',
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state): void {
    $this->configuration['body'] = $form_state->getValue('body');
  }

  /**
   * {@inheritdoc}
   */
  public function build(): array {
    $current_request = $this->requestStack->getCurrentRequest();
    $node = $current_request->attributes->get('node');
    $templateId = $this->configFactory->get('starshot_core.settings')->get('drupal_cms_id');
    $reference = $this->configFactory->get('starshot_core.settings')->get('widget_reference');
    if ($node instanceof NodeInterface && $node->bundle() == "templates") {
      $templateId = $node->id();
    }
    else {
      $node = $this->entityTypeManager->getStorage('node')->load($templateId);
    }

    if ($node instanceof NodeInterface && $node->bundle() == "space") {
      $templateId = $node->field_default_template->entity ? $node->field_default_template->entity->id() : NULL;
      if (!$templateId) {
        $build['#cache']['max-age'] = 0;
        return $build;
      }
    }

    $build = [
      '#theme' => 'drupal_cms_form',
      '#widget' => [
        'host' => $current_request->getSchemeAndHttpHost(),
        'templateId' => $templateId ?? NULL,
        'reference' => $reference ?? NULL,
        'body' => $this->configuration['body']['value'] ?? "KHA",

      ],
    ];

    $build['#cache']['max-age'] = 0;
    return $build;
  }

}
