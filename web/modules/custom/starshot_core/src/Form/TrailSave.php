<?php

declare(strict_types=1);

namespace Drupal\starshot_core\Form;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use GuzzleHttp\ClientInterface;
use GuzzleHttp\Exception\ServerException;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

/**
 * Provides a Startshot core form.
 */
final class TrailSave extends FormBase {

  /**
   * The config factory to load config from.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;
  /**
   * The entity type manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The request stack.
   *
   * @var \Symfony\Component\HttpFoundation\RequestStack
   */
  protected $requestStack;

  /**
   * The HTTP Client service.
   *
   * @var \GuzzleHttp\ClientInterface
   */
  protected $httpClient;

  /**
   * Contruct payment controler.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The factory for configuration objects.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager service.
   * @param \Symfony\Component\HttpFoundation\RequestStack $request_stack
   *   The request stack.
   * @param \GuzzleHttp\ClientInterface $http_client
   *   The client used to submit to engines.
   */
  public function __construct(
    ConfigFactoryInterface $config_factory,
    EntityTypeManagerInterface $entity_type_manager,
    RequestStack $request_stack,
    ClientInterface $http_client,
  ) {
    $this->configFactory = $config_factory;
    $this->entityTypeManager = $entity_type_manager;
    $this->requestStack = $request_stack;
    $this->httpClient = $http_client;

  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('entity_type.manager'),
      $container->get('request_stack'),
      $container->get('http_client'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return 'starshot_core_trail_save';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, $submission_id = NULL): array {
    $submissions = $this->entityTypeManager->getStorage('webform_submission')->loadByProperties(['uuid' => $submission_id]);
    if (!$submissions) {
      throw new AccessDeniedHttpException();
    }
    $submission = reset($submissions);

    $form['email'] = [
      '#type' => 'email',
      '#title' => $this->t('Your Email'),
      '#required' => TRUE,
    ];
    $form['include_dev'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Include Cloud Dev environment?'),
      '#default_value' => $this->requestStack->getCurrentRequest()->query->get('include_dev', FALSE) == 'true',
    ];
    $form['agree'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('I Agree with'),
      '#required' => TRUE,
      '#description' => $this->t('
        <a href="@term_and_condition" target="_blank">Terms & Conditions</a>,
        <a href="@acceptable_use_policy" target="_blank">Acceptable Use Policy</a>,
        <a href="@privacy_policy" target="_blank">Privacy Policy</a>, and
        <a href="@cookie_policy" target="_blank">Cookie Policy</a>', [
          '@term_and_condition' => '/terms-and-conditions',
          '@acceptable_use_policy' => '/acceptable-use-policy',
          '@privacy_policy' => '/privacy-policy',
          '@cookie_policy' => '/cookie-policy',
        ]),
    ];

    $form['actions'] = [
      '#type' => 'actions',
      'submit' => [
        '#type' => 'submit',
        '#value' => $this->t('Submit'),
      ],
    ];
    $form['#submission'] = $submission;
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state): void {
    // @todo Validate the form here.
    $email_pattern = "/^[A-Za-z0-9+_\.-]+@[A-Za-z0-9\.-]+/";
    if (!preg_match($email_pattern, $form_state->getValue('email'))) {
      $form_state->setErrorByName(
        'message',
        $this->t('Email is invalid.'),
      );
    }

  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void {
    $endpoint = $this->configFactory->get('starshot_core.settings')->get('apiEndpoint');
    $endpoint .= '/api/v2/drupal-forge/application/view?submissionId=' . $submission->uuid();
    $accessToken = $this->configFactory->get('starshot_core.settings')->get('apiToken');

    try {
      $response = $this->httpClient->request('GET', $endpoint, [
        'headers' => [
          'Authorization' => $accessToken,
          'Content-Type' => 'application/json',
        ],
      ]);
      $response = $response->getBody()->getContents();
      $response = json_decode($response);

    }
    catch (ServerException $e) {
      $this->messenger()->addMessage($this->t("Can't connect to devpanel"), 'error');
    }
    catch (\Exception $e) {
      $this->messenger()->addMessage($e->getMessage(), 'error');
    }
  }

}
