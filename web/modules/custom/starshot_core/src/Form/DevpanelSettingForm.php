<?php

declare(strict_types=1);

namespace Drupal\starshot_core\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Configure Startshot core settings for this site.
 */
final class DevpanelSettingForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return 'starshot_core_devpanel_setting';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames(): array {
    return ['starshot_core.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state): array {
    $form['apiEndpoint'] = [
      '#type' => 'textfield',
      '#title' => $this->t('API endpoint'),
      '#default_value' => $this->config('starshot_core.settings')->get('apiEndpoint'),
      '#required' => TRUE,
    ];
    $form['apiToken'] = [
      '#type' => 'password',
      '#title' => $this->t('API token'),
      '#placeholder' => $this->config('starshot_core.settings')->get('apiToken') ? str_repeat('●', 32) : '',
    ];
    $form['clusterId'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Default cluster ID'),
      '#default_value' => $this->config('starshot_core.settings')->get('clusterId'),
      '#required' => TRUE,
    ];
    $form['workspaceId'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Default Workspace ID'),
      '#default_value' => $this->config('starshot_core.settings')->get('workspaceId'),
      '#required' => TRUE,
    ];
    $form['drupal_cms_id'] = [
      '#type' => 'number',
      '#title' => $this->t('Drupal CMS ID'),
      '#default_value' => $this->config('starshot_core.settings')->get('drupal_cms_id'),
      '#required' => TRUE,
    ];
    $form['widget_reference'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Widget Reference'),
      '#default_value' => $this->config('starshot_core.settings')->get('widget_reference'),
      '#required' => FALSE,
    ];
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void {
    $config = $this->config('starshot_core.settings');

    $config->set('apiEndpoint', $form_state->getValue('apiEndpoint'));
    if ($form_state->getValue('apiToken')) {
      $config->set('apiToken', $form_state->getValue('apiToken'));
    }
    $config->set('clusterId', $form_state->getValue('clusterId'));
    $config->set('workspaceId', $form_state->getValue('workspaceId'));
    $config->set('drupal_cms_id', $form_state->getValue('drupal_cms_id'));
    $config->set('widget_reference', $form_state->getValue('widget_reference'));
    $config->save();
    parent::submitForm($form, $form_state);
  }

}
