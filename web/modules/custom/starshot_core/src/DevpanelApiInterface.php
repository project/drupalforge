<?php

declare(strict_types=1);

namespace Drupal\starshot_core;

/**
 * Add interface for devpanel api.
 */
interface DevpanelApiInterface {

  /**
   * Set api endpoint.
   */
  public function setEndPoint($end_point);

  /**
   * Set api method.
   */
  public function method($method);

  /**
   * Get api error.
   */
  public function getError();

  /**
   * Execute API.
   */
  public function execute();

  /**
   * Get an application.
   */
  public function getApplication($submissionId);

  /**
   * Get an application by id.
   */
  public function getApplicationById($applicationId);

}
