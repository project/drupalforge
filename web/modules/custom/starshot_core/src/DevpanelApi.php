<?php

declare(strict_types=1);

namespace Drupal\starshot_core;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use GuzzleHttp\ClientInterface;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\ServerException;

/**
 * Class DevpanelApi to call to devpanel service.
 */
final class DevpanelApi implements DevpanelApiInterface {

  /**
   * The api endpoint.
   *
   * @var string endPoint
   */
  protected $endPoint;
  /**
   * The api access token.
   *
   * @var string accessToken
   */
  protected $accessToken;
  /**
   * The api get params.
   *
   * @var array params
   */
  protected $params;
  /**
   * The api post body.
   *
   * @var array body
   */
  protected $body;
  /**
   * The api error.
   *
   * @var string error
   */
  protected $error;

  /**
   * The config factory to load config from.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;
  /**
   * The entity type manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;


  /**
   * The HTTP Client service.
   *
   * @var \GuzzleHttp\ClientInterface
   */
  protected $httpClient;

  /**
   * Contruct payment controler.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The factory for configuration objects.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager service.
   * @param \GuzzleHttp\ClientInterface $http_client
   *   The client used to submit to engines.
   */
  public function __construct(
    ConfigFactoryInterface $config_factory,
    EntityTypeManagerInterface $entity_type_manager,
    ClientInterface $http_client,
  ) {
    $this->configFactory = $config_factory;
    $this->entityTypeManager = $entity_type_manager;
    $this->httpClient = $http_client;
    $this->accessToken = $this->configFactory->get('starshot_core.settings')->get('apiToken');
    $this->method = "GET";
    $this->body = [];
  }

  /**
   * {@inheritdoc}
   */
  public function setEndPoint($end_point) {
    $this->endPoint = $this->configFactory->get('starshot_core.settings')->get('apiEndpoint');
    $this->endPoint .= $end_point;
  }

  /**
   * {@inheritdoc}
   */
  public function method($method) {
    $this->method = $method;
  }

  /**
   * {@inheritdoc}
   */
  public function getError() {
    return $this->error;
  }

  /**
   * {@inheritdoc}
   */
  public function execute() {

    try {
      $request_params = [
        'headers' => [
          'Authorization' => $this->accessToken,
          'Content-Type' => 'application/json',
        ],
      ];
      if ($this->method == 'POST') {
        $request_params['body'] = json_decode($this->body);
      }
      $response = $this->httpClient->request($this->method, $this->endPoint, $request_params);
      $response = $response->getBody()->getContents();
      $response = json_decode($response);
      return $response;
    }
    catch (ServerException $e) {
      $this->error = "Can't connect to DevPanel service";
    }
    catch (ClientException $e) {
      $error = $e->getResponse();
      $error = $error ? $error->getBody() : NULL;
      $error = $error ? $error->getContents() : NULL;
      $error = $error ? json_decode($error) : NULL;
      if (isset($error->message)) {
        $error_message = is_array($error->message) ? implode(', ', $error->message) : $error->message;
      }
      else {
        $error_message = $e->getMessage();
      }
      $this->error = ucfirst($error_message);
    }
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function getApplication($submissionId) {
    $endpoint = '/api/v2/drupal-forge/application/get/' . $submissionId;
    $this->setEndPoint($endpoint);
    return $this->execute();
  }

  /**
   * {@inheritdoc}
   */
  public function getApplicationById($applicationId) {
    $endpoint = '/api/v2/drupal-forge/application/id/' . $applicationId;
    $this->setEndPoint($endpoint);
    return $this->execute();
  }

}
