<?php

namespace Drupal\starshot_cron\Drush\Commands;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drush\Attributes as CLI;
use Drush\Commands\DrushCommands;
use GuzzleHttp\ClientInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * A Drush commandfile.
 */
final class DrupalForgeRebuildApplicationCommands extends DrushCommands {

  /**
   * The config factory to load config from.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;
  /**
   * The entity type manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The HTTP Client service.
   *
   * @var \GuzzleHttp\ClientInterface
   */
  protected $httpClient;

  /**
   * Constructs a DrupalForgeRebuildApplicationCommands object.
   */
  public function __construct(
    private readonly ConfigFactoryInterface $config_factory,
    private readonly EntityTypeManagerInterface $entity_type_manager,
    private readonly ClientInterface $http_client,
  ) {
    parent::__construct();
    $this->configFactory = $config_factory;
    $this->entityTypeManager = $entity_type_manager;
    $this->httpClient = $http_client;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('entity_type.manager'),
      $container->get('http_client'),
    );
  }

  /**
   * Rebuild DrupalForge application.
   */
  #[CLI\Command(name: 'starshot_cron:rebuild-applicaton', aliases: ['cra'])]
  #[CLI\Usage(name: 'starshot_cron:rebuild-applicaton', description: 'Rebuild drupal forge application')]
  public function rebuildApplication() {
    $templates = $this->entityTypeManager->getStorage('node')->loadByProperties([
      'field_auto_rebuild' => TRUE,
      'type' => 'templates',

    ]);
    if (!$templates) {
      return FALSE;
    }
    $accessToken = \Drupal::config('starshot_core.settings')->get('apiToken');
    $endpoint = \Drupal::config('starshot_core.settings')->get('apiEndpoint');
    $endpoint .= '/api/v2/drupal-forge/admin/quick-start/rebuild';
    foreach ($templates as $entity) {
      $data = [
        'templateId' => $entity->field_devpanel_id->value,
      ];
      try {
        $client = \Drupal::httpClient();
        $client->request('POST', $endpoint, [
          'body' => json_encode($data),
          'headers' => [
            'Authorization' => $accessToken,
            'Content-Type' => 'application/json',
          ],
        ]);
      }
      catch (\Exception $e) {
        $this->logger()->error($e->getMessage());
        return FALSE;
      }
    }
    $this->logger()->success(dt('Rebuild sucessfully'));
  }

}
